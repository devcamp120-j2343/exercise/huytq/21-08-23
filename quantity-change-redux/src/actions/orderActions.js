import { IPHONE_QUANTITY_CLICKED, NOKIA_QUANTITY_CLICKED, SAMSUNG_QUANTITY_CLICKED } from "../constants/orderContants"


export const iphoneQuantityClickAction = (inputValue) => {
    return {
        type: IPHONE_QUANTITY_CLICKED,

    }
}

export const samsungQuantityClickAction = (inputValue) => {
    return {
        type: SAMSUNG_QUANTITY_CLICKED,
    }
}

export const nokiaQuantityClickAction = (inputValue) => {
    return {
        type: NOKIA_QUANTITY_CLICKED,
    }
}