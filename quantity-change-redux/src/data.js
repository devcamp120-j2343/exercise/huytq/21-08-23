 const mobileList = [
    {
      name: "IPhone X",
      price: 900
    },
    {
      name: "Samsung S9",
      price: 800
    },
    {
      name: "Nokia 8",
      price: 650
    }
  ]
  
  export default mobileList;