// Root reducer
import { combineReducers } from "redux";

// Khai báo các reducer con
import orderReducer from "./orderReducer";

// Tạo một root reducer 
const rootReducer = combineReducers({
    orderReducer
});

export default rootReducer;