import { IPHONE_QUANTITY_CLICKED, NOKIA_QUANTITY_CLICKED, SAMSUNG_QUANTITY_CLICKED } from "../constants/orderContants";
import mobileList from "../data";

// Định nghĩa state khởi tạo cho order
const initialState = {
    iphoneQuantity: 0,
    samsungQuantity: 0,
    nokiaQuantity: 0,
    total: 0,
}

// Định nghĩa khai báo task reducer
const orderReducer = (state = initialState, action) => {
    // Update state
    switch (action.type) {
        case IPHONE_QUANTITY_CLICKED:
            state.iphoneQuantity += 1;
            state.total += mobileList[0].price;
            break;
        case SAMSUNG_QUANTITY_CLICKED:
            state.samsungQuantity += 1;
            state.total += mobileList[1].price;
            break;
        case NOKIA_QUANTITY_CLICKED:
            state.nokiaQuantity += 1;
            state.total += mobileList[2].price;
            break;
        default:
            break;
    }

    return { ...state };
}

export default orderReducer;