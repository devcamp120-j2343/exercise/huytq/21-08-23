import { Container, Grid, Box, Button } from "@mui/material";
import mobileList from "../data";
//redux
import { useDispatch, useSelector } from "react-redux";
import { iphoneQuantityClickAction , samsungQuantityClickAction, nokiaQuantityClickAction} from "../actions/orderActions";

function Order() {
  // Khai báo hàm dispatch sự kiện tới redux store
  const dispatch = useDispatch();

  // useSelector để đọc state từ redux
  const { iphoneQuantity, samsungQuantity, nokiaQuantity, total, } = useSelector((reduxData) => {
    return reduxData.orderReducer;
  })

  const iphoneQuantityClickHandler = () => {
    dispatch(iphoneQuantityClickAction());
  }

  const samsungQuantityClickHandler = () => {
    dispatch(samsungQuantityClickAction());
  }

  const nokiaQuantityClickHandler = () => {
    dispatch(nokiaQuantityClickAction());
  }


  return (
    <>
      <Container>
        <h1>Mobile Order</h1>
        <Grid container spacing={4} mt={5}>
          <Grid item md={4}>
            <Box sx={{ p: 5, border: '1px solid grey' }}>
              <h5>{mobileList[0].name}</h5>
              <p>Price: {mobileList[0].price} USD</p>
              <p>Quantity: {iphoneQuantity}</p>
              <Button sx={{ bgcolor: "green", color: "white",   
                 '&:hover': {
                    backgroundColor: 'gray',
                    color: 'white',
                  },
              }} onClick={iphoneQuantityClickHandler}>Buy</Button>
            </Box>
          </Grid>
          <Grid item md={4}>
            <Box sx={{ p: 5, border: '1px solid grey' }}>
              <h5>{mobileList[1].name}</h5>
              <p>Price: {mobileList[1].price} USD</p>
              <p>Quantity: {samsungQuantity}</p>
              <Button sx={{ bgcolor: "green", color: "white", 
                '&:hover': {
                    backgroundColor: 'gray',
                    color: 'white',
                  },
              }} onClick={samsungQuantityClickHandler}>Buy</Button>
            </Box>
          </Grid>
          <Grid item md={4}>
            <Box sx={{ p: 5, border: '1px solid grey' }}>
              <h5>{mobileList[2].name}</h5>
              <p>Price: {mobileList[2].price} USD</p>
              <p>Quantity: {nokiaQuantity}</p>
              <Button sx={{ bgcolor: "green", color: "white",
                '&:hover': {
                    backgroundColor: 'gray',
                    color: 'white',
                  },
               }} onClick={nokiaQuantityClickHandler}>Buy</Button>
            </Box>
          </Grid>
        </Grid>
        <h4>Total: {total} USD</h4>
      </Container>
    </>
  );
}

export default Order;
