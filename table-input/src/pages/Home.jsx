import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
const Home = () => {
  const [inputs, setInputs] = useState({
    name: "",
  });
  const [tableData, setTableData] = useState([]);
  const [editClick, setEditClick] = useState(false);
  const [editIndex, setEditIndex] = useState("");
  const handleChange = (e) => {
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    
    console.log("Thêm dòng", inputs);
    if (editClick) {
      const tempTableData = tableData;
      Object.assign(tempTableData[editIndex], inputs);
      setTableData([...tempTableData]);
      setEditClick(false);
      setInputs({
        name: "",
        
      });
    } else {
      setTableData([...tableData, inputs]);
      setInputs({
        name: "",
        
      });
    }
  };

  const handleDelete = (index) => {
    const filterData = tableData.filter((item, i) => i !== index);
    setTableData(filterData);
  };
  const handleEdit = (index) => {
    const tempData = tableData[index];
    console.log(tempData);
    setInputs({ name: tempData.name });
    setEditClick(true);
    setEditIndex(index);
  };
  const [search,setSearch] = useState(tableData);
  function filterName(event) {
    const newData = tableData.filter(row => {
      return row.name.toLowerCase().includes(event.target.value.toLowerCase())    
    })
    setSearch(newData)
  }
  return (
    <div className=" container mt-4 text-center">
      <div className=" row border-3 m-auto d-flex flex-row " style={{padding:"10px"}}>
        <form className="form-control" onSubmit={handleSubmit}>
          <div className=" row">
              <div className=" col-3">
                <label className=" form-control">Nhập nội dung dòng: </label>
              </div>

              <div className=" col-5">
                <input className=" form-control" name="name" value={inputs.name} onChange={handleChange} />
              </div>

              <div className=" col-4">
                <button type="submit" className=" btn text-dark w-50 " style={{border:" solid 1px "}}>
                  {editClick ? "update" : "Thêm"}
                </button>
              </div>
          </div>
          
        </form>
        <div className=" row mt-4">
              <div className=" col-3">
                <label className=" form-control">Nhập nội dung lọc: </label>
              </div>

              <div className=" col-5">
                <input type="text" className=" form-control" value={search}  onChange={(e) => filterName(e)}/>
              </div>

              <div className=" col-4">
                <button className=" btn text-dark w-50" style={{border:" solid 1px "}}>
                  Lọc
                </button>
              </div>
          </div>
      </div>
      <div>
        <table className=" table table-bordered table-striped w-full  text-center mt-4">
          <thead >
            <tr >
              <th style={{backgroundColor:"gray"}} >STT</th>
              <th style={{backgroundColor:"gray"}}>Name</th>
              <th style={{backgroundColor:"gray"}}>Actions</th>
            </tr>
          </thead>
          <tbody className="text-white">
            {tableData.map((item, i) => (
              <tr key={i}>
                <td>{i + 1}</td>
                <td>{item.name}</td>
                <td>
                  <button
                    onClick={() => handleEdit(i)}
                    className="btn mr-3  text-primary"
                  >
                    Edit
                  </button>
                  <button
                    onClick={() => handleDelete(i)}
                    className=" btn text-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Home;
