import { ADD_TASK_CLICKED, INPUT_TASK_NAME, TOGGLE_STATUS_TASK } from "../constants/task.constants";

// Định nghĩa state khởi tạo cho task
// const initialState = {
//     inputString: "",
//     taskList: [{
//         taskName: "",
//         status: true
//     }]
// }
const initialState = {
    inputString: "",
    taskList: []
}

// Định nghĩa khai báo task reducer
const taskReducer = (state = initialState, action) => {
    // Update state
    switch (action.type) {
        case INPUT_TASK_NAME:
            state.inputString = action.payload;
            break;
        case ADD_TASK_CLICKED:
            state.taskList.push({
                taskName: state.inputString,
                status: false
            });

            state.inputString = "";
            break;
        case TOGGLE_STATUS_TASK:
            const index = action.payload;

            state.taskList[index].status = !state.taskList[index].status;
            break;
        default:
            break;
    }

    return {...state};
}

export default taskReducer;