// Root reducer
import { combineReducers } from "redux";

// Khai báo các reducer con
import taskReducer from "./task.reducer";

// Tạo một root reducer 
const rootReducer = combineReducers({
    taskReducer,
    // Cacs reducer con khác nếu có
});

export default rootReducer;