import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// Import rootReducer để tạo store
import rootReducer from './reducers';
// Import hàm để tạo Store cho redux
import { createStore } from 'redux';

// Là 1 component kết nối giữa react và redux (Dịch vụ)
import { Provider } from 'react-redux';

// Hàm để tạo store
const store = createStore(rootReducer);

const root = ReactDOM.createRoot(document.getElementById('root'));

// Component Provider cần bao ngoài App để Các component trong app có thể đọc state từ store
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
